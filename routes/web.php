<?php

use App\Http\Controllers\ContactsController;
use App\Http\Controllers\ApplicantsController;
use App\Http\Controllers\BackupController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\DealController;
use App\Http\Controllers\DriveController;
use App\Http\Controllers\EvaluationController;
use App\Http\Controllers\InternshipsController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\QuarterController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TeamController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';
Route::group(['middleware'=>'auth'],function(){
    Route::view('/dashboard','dashboard')->name('dashboard');
    Route::view('/home', 'welcome')->name('home');
    Route::resource('/contacts'     ,ContactsController::class);
    Route::resource('/teams'        ,TeamController::class);
    Route::resource('/projects'     ,ProjectController::class);
    Route::resource('/tasks'        ,TaskController::class);
    Route::resource('/courses'       ,CourseController::class);
    Route::resource('/invoices'     ,InvoiceController::class);
    Route::resource('/quarters'     ,QuarterController::class);
    Route::resource('/modules'      ,ModuleController::class);
    Route::resource('/evaluations'  ,EvaluationController::class);
    Route::resource('/deals'        ,DealController::class);
    Route::resource('/companies'    ,CompanyController::class);
    Route::resource('/roles'        ,RoleController::class);
    Route::resource('/applicants'   ,ApplicantsController::class);
    Route::resource('/internships'  ,InternshipsController::class);
    Route::get('/applicants_import',[ApplicantsController::class,'import']);
    Route::get('/applicants_export',[ApplicantsController::class,'export'])->name('applicants.export');
    Route::get('/contacts_import',[ContactsController::class,'import']);
    Route::get('/contacts_export',[ContactsController::class,'export'])->name('contacts.export');


    Route::put('/internship_applicant/{id}/update_status',[InternshipsController::class,'update_status'])->name('internship.update_status');
});

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/testdrive',[DriveController::class , 'index']);

    // // Backup routes
    // Route::get('backup',  [BackupController::class,'index']);
    // Route::get('backup/create',  [BackupController::class,'create']);
    // Route::get('backup/download/{file_name}', [BackupController::class,'download']);
    // Route::get('backup/delete/{file_name}',  [BackupController::class,'delete']);
