<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternshipApplicantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internship_applicant', function (Blueprint $table) {
            $table->id();
            $table->foreignId('internship_id')->nullable()->constrained('internships')->onDelete('cascade');
            $table->foreignId('applicant_id')->nullable()->constrained('applicants')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internship_applicant');
    }
}
