<?php

namespace Database\Factories;

use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'title'=>$this->faker->sentence($nbWords = 5, $variableNbWords = true) ,
            'description'=>$this->faker->sentence,
            'due_date'=>$this->faker->dateTimeBetween('now','3 years',null)
        ];
    }
}
