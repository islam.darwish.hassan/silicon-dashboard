<x-app-layout>
    @php
        $create = ['btn_name' => 'Add Applicant  ', 'type' => 'modal', 'method' => 'POST', 'name' => 'Add New Applicant', 'route' => [$name . '.store', [$name]], 'action_route' => [$name . '.store', [$name]]];
    @endphp
    <x-slot name="content">
        <x-top-nav>
            <div class="d-block mb-4 mb-md-0">
                <x-top-nav-bread>
                    <li class="breadcrumb-item active"><a
                            href="{{ route($name . '.index') }}">{{ ucfirst($name ?? '') }}</a></li>
                </x-top-nav-bread>
                <h2 class="h4 mb-0">{{ ucfirst($name ?? '') }} </h2>
                <p class="mb-2">This {{ $name ?? '' }} dashboard to manage it .</p>
            </div>
        </x-top-nav>

        <x-action-modal-btn :operation="$create"><span class="fas fa-plus me-2"></span></x-action-modal-btn>
        <x-action-modal :operation="$create">
            @include('forms.applicants.create')
            <div class="d-grid">
                <x-button type="submit" class="btn btn-dark">Add New Applicant </x-button>
            </div>
        </x-action-modal>
        <x-searchbar route="{{ route($name . '.index') }}">
            <div class="d-flex justify-content-end">
                <a href={{ route('applicants.export') }} class=' btn btn-sm btn-primary   pe-1 text-white  '><span
                        class="fas fa-file-csv me-1"></span>
                    Export Report
                </a>
            </div>

        </x-searchbar>

        <x-table>
            <x-slot name="header">
                <x-table-h data-priority="0">#</x-table-h>
                <x-table-h data-priority="1">CV</x-table-h>
                <x-table-h data-priority="1">Full Name</x-table-h>
                <x-table-h data-priority="0">Email</x-table-h>
                <x-table-h data-priority="0">Phone</x-table-h>
                <x-table-h data-priority="1">Age</x-table-h>
                <x-table-h data-priority="1">City</x-table-h>
                <x-table-h data-priority="1">University</x-table-h>
                <x-table-h data-priority="1">Major</x-table-h>
                <x-table-h data-priority="1">Internships</x-table-h>

            </x-slot>
            <x-slot name="body">
                @foreach ($data as $row)
                    <tr>
                        <x-table-b>{{ $row->id }}</x-table-b>
                        <x-table-b><a href={{ $row->cv }} target="_blank" class="text-success">View</a></x-table-b>
                        <x-table-b>{{ $row->name }}</x-table-b>
                        <x-table-b>{{ $row->email }}</x-table-b>
                        <x-table-b>{{ $row->phone }}</x-table-b>
                        <x-table-b>{{ $row->age }}</x-table-b>
                        <x-table-b>{{ $row->city }}</x-table-b>
                        <x-table-b>{{ $row->uni }}</x-table-b>
                        <x-table-b>{{ $row->major }}</x-table-b>
                        <x-table-b>
                            @foreach ($row->internships as $item)
                                <div class="badge bg-dark">{{ Str::ucfirst($item->name) }}</div>
                            @endforeach

                        </x-table-b>

                    </tr>

                @endforeach

            </x-slot>
            <x-slot name="footer">
                {{ $data->links('vendor.pagination.bootstrap-4') }}

            </x-slot>

        </x-table>
    </x-slot>
</x-app-layout>
