<x-guest-layout>
    <x-auth-card>
        <x-slot name="title">
            <div class="text-center text-md-center mb-4 mt-md-0">
                <h1 class="mb-0 h3">Sign in to our platform</h1>
            </div>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->

            <div class="form-group mb-4">
                <x-label for="email" :value="__('Email')" />
                <div class="input-group">
                    <span class="input-group-text" id="basic-addon1"><span class="fas fa-envelope"></span></span>
                    <x-input id="password" 
                                    type="email"
                                    name="email"
                                    :value="old('email')" required autofocus />
                </div>
            </div>


            <!-- Password -->
            <div class="form-group mb-4">
                <x-label for="password" :value="__('Password')" />
                <div class="input-group">
                    <span class="input-group-text" id="basic-addon1"><span class="fas fa-unlock-alt"></span></span>
                    <x-input id="password" 
                                    type="password"
                                    name="password"
                                    required autocomplete="current-password" />
                </div>
            </div>

            <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                <div class="d-grid">
                <x-button class="ml-3">
                    {{ __('Log in') }}
                </x-button>
                 </div>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
