<div class="mb-2" ><a class="btn btn-primary btn-sm" href="{{ url()->previous() }}
    "><i class="fas fa-arrow-left fa-xs"></i><small> Previous</small></a>
</div>
<nav aria-label="breadcrumb" class="d-none d-md-inline-block">
    <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">

        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><span class="fas fa-home"></span></a></li>
        {{ $slot }}
    </ol>
</nav>

