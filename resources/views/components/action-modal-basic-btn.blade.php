@props(['operation'])
<a  {{ $attributes->merge(['type' => 'button', 'class' => ' text-white p-1']) }}  data-bs-toggle="modal" data-bs-target="#{{'modal-'.$operation['route'][1][0]}}">
  {{$slot}}
  </a>
