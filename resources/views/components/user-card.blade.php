@props(['user'=>null])
<div class="col-lg-4 col-md-6  mb-4 mt-1">
  <div class="card shadow-sm text-center p-0">
      <div class="profile-cover rounded-top" data-background="{{asset('/img/profile-cover.jpg')}}"></div>
      <div class="card-body pb-5">
          <img src="{{asset('img/team/profile-picture-1.jpg')}}" class="user-avatar large-avatar rounded-circle mx-auto mt-n7 mb-4" alt="Neil Portrait">
          <h4 class="h3">{{$user->name}}</h4>
          <h5 class="fw-normal">Senior Software Engineer</h5>
          <p class="text-gray mb-4">New York, USA</p>
          <a class="btn btn-sm btn-dark me-2" href="#"><span class="fas fa-user-plus me-1"></span> Connect</a>
          <a class="btn btn-sm btn-secondary" href="#">Show Profile</a>
      </div>
   </div>
</div>
