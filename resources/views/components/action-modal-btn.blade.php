@props(['operation'])
<button  {{ $attributes->merge(['type' => 'button', 'class' => 'btn btn-sm btn-primary mb-3']) }}  data-bs-toggle="modal" data-bs-target="#{{'modal-'.$operation['route'][1][0]}}">
  {{$slot}}
  {{$operation['btn_name']??$operation['name']}}</button>
