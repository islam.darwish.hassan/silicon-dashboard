@props(['project'=>null])
<div class="col-lg-4 col-md-6  mb-4 mt-1">
  <div class="card shadow-sm  p-0">
    <div class="project-cover rounded-top p-2 bg-gradient-dark text-center" >
        <img src="{{asset('img/team/profile-picture-3.jpg')}}" class=" avatar-xl rounded-circle mt-2 " alt="{{$project->name}}">
        <h4 class="h4 mt-2 text-white">{{Str::limit($project->title,30)}}</h4>
    </div>
      <div class="card-body pb-5">
          <h5 class="fw-normal pt-0">{{$project->caption}}</h5>
          <p class="text-gray mb-4">New York, USA</p>
          <a class="btn btn-sm btn-dark me-2" href="#"><span class="fas fa-user-plus me-1"></span> Connect</a>
          <a class="btn btn-sm btn-secondary" href="#">Show Profile</a>
      </div>
   </div>
</div>
