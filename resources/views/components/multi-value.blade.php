@props(['values' => [], 'value' => 0, 'classes' => []])

@for ($i = 1; $i <= count($values); $i++)
    @if ($value == $i)
        <span class="@if(isset($classes[$i-1])){{$classes[$i-1]}} @endif">{{ $values[$i - 1] }}
        </span>
    @endif
@endfor
