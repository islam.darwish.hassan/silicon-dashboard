<nav id="sidebarMenu" class="sidebar d-md-block bg-dark text-white collapse" data-simplebar>
    <div class="sidebar-inner px-4 pt-3">
      <div class="user-card d-flex d-md-none align-items-center justify-content-between justify-content-md-center pb-4">
        <div class="d-flex align-items-center">
          <div class="user-avatar lg-avatar me-4">
            <img src="{{asset('img/team/eslam.jpg')}}" class="card-img-top rounded-circle border-white"
              alt="{{auth()->user()->name}}">
          </div>
          <div class="d-block">
            <h2 class="h6">Hi, {{auth()->user()->name}}</h2>
            <a href="{{ route('logout') }}" 
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"
            class="btn btn-secondary text-dark btn-xs"><span
                class="me-2"><span class="fas fa-sign-out-alt"></span></span>Sign Out</a>
          </div>
        </div>
        <div class="collapse-close d-md-none">
          <a href="#sidebarMenu" class="fas fa-times" data-bs-toggle="collapse" data-bs-target="#sidebarMenu"
            aria-controls="sidebarMenu" aria-expanded="true" aria-label="Toggle navigation"></a>
        </div>
      </div>
      <ul class="nav flex-column pt-3 pt-md-0">
        <li class="nav-item">
          <a href="{{route('home')}}" class="nav-link d-flex align-items-center">
            <span class="sidebar-icon">
              <img src="{{asset('img/brand/light.svg')}}" height="30" width="30" alt="Arena Logo">
            </span>
            <span class="mt-1 ms-1 sidebar-text">Silicon-Arena</span>
          </a>
        </li>
        <li role="separator" class="dropdown-divider mb-3 border-black"></li>
        {{$slot}}
      </ul>
    </div>
  </nav>




  {{-- <li role="separator" class="dropdown-divider mt-4 mb-3 border-black"></li>
  <li class="nav-item">
    <a href="https://themesberg.com/docs/volt-bootstrap-5-dashboard/getting-started/quick-start/" target="_blank"
      class="nav-link d-flex align-items-center">
      <span class="sidebar-icon"><span class="fas fa-book"></span></span>
      <span class="sidebar-text">Documentation <span
          class="badge badge-md bg-secondary ms-1 text-dark">v1.3</span></span>
    </a>
  </li> --}}
