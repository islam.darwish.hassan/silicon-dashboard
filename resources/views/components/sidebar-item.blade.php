@props(['activelink'=>false])

<li  class="nav-item @if($activelink) active @endif" >
  {{ $slot}}
</li>