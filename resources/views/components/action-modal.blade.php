    <!-- Very little is needed to make a happy life. - Marcus Antoninus -->
    <div class="modal fade" id="{{'modal-'.$operation['route'][1][0]}}"  tabindex="-1" role="dialog" aria-labelledby="{{'modal-'.$operation['route'][1][0]}}" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered  @if ($large) modal-lg @endif " role="document">
            <div class="modal-content">
                <form method="POST" action={{ route($operation['action_route'][0],$operation['action_route'][1]) }}>
                    @csrf
                    @if($operation['method']=="PUT")
                    @method('PUT')
                    @endif
                    <div class="modal-body p-0">
                        <div class="card p-3 p-lg-4">
                            <button type="button" class="btn-close ms-auto" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div class="card-header border-0 text-center pb-0">
                                <h2 class="mb-3 h5">{{$operation['name']?? ''}}</h2>
                            </div>
                            <div class="card-body p-0 pl-lg-3">
                            {{$slot}}

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
     </div>
