@props(['header'=>'','activelink'=>false,'icon'=>null])
<li class="nav-item @if($activelink) active @endif">
        <span
        class="nav-link  collapsed  d-flex justify-content-between align-items-center"
        data-bs-toggle="collapse" data-bs-target="#{{$header}}">
        <span>
          <span class="sidebar-icon"><span class="{{$icon??'fas fa-box-open'}}"></span></span>
          <span class="sidebar-text">{{$header}}</span>
        </span>
        <span class="link-arrow"><span class="fas fa-chevron-right"></span></span>
      </span>
    <div class="multi-level collapse " role="list"
      id="{{$header}}" aria-expanded="false">
      <ul class="flex-column nav">
        {{$slot}}
      </ul>
    </div>
  </li>