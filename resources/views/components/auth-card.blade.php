<div class="container">
    <p class="text-center"><a href="../dashboard/dashboard.html" class="text-gray-700"><i
                class="fas fa-angle-left me-2"></i> Visit Website</a></p>
    <div class="row justify-content-center form-bg-image"
        data-background-lg="{{ asset('img/illustrations/signin.svg') }}">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="bg-white shadow-soft border rounded border-light p-4 p-lg-5 w-100 fmxw-500">
                {{ $title }}
                {{ $slot }}
                <div class="d-flex justify-content-center align-items-center mt-4">
                    <span class="fw-normal">
                        Contact Us 
                        <a  class="fw-bold">hello@silicon-arena.com</a>
                    </span>
                </div>

            </div>
            
        </div>
        
    </div>
</div>
