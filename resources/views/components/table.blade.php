<div class="card border-light shadow-sm mb-4">
    <div class="card-body rwd-table">
        {{$filters??""}}

        <div class="table-responsive " data-pattern="priority-columns" data-add-focus-btn="" >

            <table class="table table-centered  table-nowrap mb-0 rounded">
                <thead class="thead-light">
                    <tr>
                        {{$header}}
                    </tr>
                </thead>
                <tbody>
                    <!-- Item -->
                        {{$body}}
                    <!-- End of Item -->

                </tbody>
            </table>

        </div>

    </div>
    <div class="d-flex justify-content-center">
    {{$footer}}
</div>
</div>
