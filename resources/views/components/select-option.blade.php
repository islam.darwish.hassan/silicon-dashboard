@props(['selected' => false])
<option {{ $selected ? 'selected' : '' }}  {!! $attributes->merge(['class' => '']) !!}>
    {{$slot}}
</option>

