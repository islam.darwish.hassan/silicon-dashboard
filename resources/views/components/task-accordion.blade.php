@props(['task'=>null,'parent'=>'' ,'show'=>false])
<div class="accordion-item">
    <h2 class="accordion-header " id="heading-{{$task->id}}">
      <button class="accordion-button @if(!$show) collapsed @endif " type="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{$task->id}}" aria-expanded="true" aria-controls="collapse-{{$task->id}}">
        {{$task->title}}
      </button>
    </h2>
    <div id="collapse-{{$task->id}}" class="accordion-collapse collapse @if($show) show @endif " aria-labelledby="heading-{{$task->id}}" data-bs-parent="#{{$parent}}">
        <div class="accordion-body">
            <span class="badge badge-lg bg-success">Success</span>
          <strong>This is the first item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
          {{$task->description}}
          
        </div>
      </div>
</div>
