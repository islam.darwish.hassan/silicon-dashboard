@props(['route' => ''])
<div class="table-settings mb-2">
    <div class="d-flex align-items-center justify-content-between ">
        <form method='get' action={{ $route }}>
            <div class="input-group" >
                <span class="input-group-text" id="basic-addon2"><span class="fas fa-search"></span></span>
                <input type="search" class="form-control" name="search_input" placeholder="Search ..."
                    value={{ request()->query('search_input') }}>
            </div>
        </form>
        {{ $slot }}

    </div>
</div>
