            <div class="form-group mb-4">
                <x-label for="name" :value="__('Name')" />
                <div class="input-group">
                    <span class="input-group-text" id="basic-addon1"><span class="fas fa-tag"></span></span>
                    <x-input id="name"
                                    type="text"
                                    name="name"
                                    :value="old('name')" required />
                </div>
            </div>
            <div class="form-group mb-4">

                <x-label for="email" :value="__('Email')" />
                <div class="input-group">
                    <span class="input-group-text" id="basic-addon1"><span class="fas fa-at"></span></span>

                    <x-input id="email"
                                    type="text"
                                    name="email"
                                    :value="old('email')" required />
                </div>
            </div>
            <div class="form-group mb-4">

                <x-label for="phone" :value="__('Phone')" />
                <div class="input-group">
                    <span class="input-group-text" id="basic-addon1"><span class="fas fa-phone-alt"></span></span>

                    <x-input id="phone"
                                    type="text"
                                    name="phone"
                                    :value="old('phone')" required />
                </div>
            </div>
            <div class="form-group mb-4">

                <x-label for="uni" :value="__('University')" />
                <div class="input-group">
                    <span class="input-group-text" id="basic-addon1"><span class="fas fa-university"></span></span>
                    <x-input id="uni"
                                    type="text"
                                    name="uni"
                                    :value="old('uni')" required />
                </div>
            </div>
            <div class="form-group mb-4">

                <x-label for="city" :value="__('City')" />
                <div class="input-group">
                    <span class="input-group-text" id="basic-addon1"><span class="fas fa-map-pin"></span></span>
                    <x-input id="city"
                                    type="text"
                                    name="phone"
                                    :value="old('city')" required />
                </div>
            </div>
            <div class="file-field mb-4">
                <div class="d-flex  ms-xl-3">
                   <div class="d-flex">
                      <span class="icon icon-md"><span class="fas fa-paperclip me-3"></span></span> <input type="file">
                      <div class="d-md-block text-left">
                         <div class="fw-normal text-dark mb-1">Upload CV</div>
                         <div class="text-gray small">Max size of 2 MB</div>
                      </div>
                   </div>
                </div>
             </div>
