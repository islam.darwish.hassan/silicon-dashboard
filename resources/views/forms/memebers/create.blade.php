            <!-- Name-->
            <div class="form-group mb-4">
                <x-label for="name" :value="__('Name')" />
                <div class="input-group">
                    <span class="input-group-text" id="basic-addon1"><span class="fas fa-user"></span></span>
                    <x-input id="name" 
                                    type="text"
                                    name="name"
                                    placeholder="Member Name"
                                    :value="old('name')" required />
                </div>
            </div>
            <!-- Job title -->
            <div class="form-group mb-4">
                <x-label for="job_title" :value="__('Member Job Title')" />
                <x-select name="job_title">
                    <x-select-option selected>Open this select menu</x-select-option>
                    <x-select-option :value="1" selected>One</x-select-option>
                    <x-select-option :value="2">Two</x-select-option>
                    <x-select-option :value="3">Three</x-select-option>
                </x-select>
            </div>
             <!-- Email Address -->
                <div class="form-group mb-4">
                    <x-label for="email" :value="__('Email')" />
                    <div class="input-group">
                        <span class="input-group-text" id="basic-addon1"><span class="fas fa-envelope"></span></span>
                        <x-input id="password" 
                                        type="email"
                                        name="email"
                                        placeholder="example@silicon-arena.com"
                                        :value="old('email')" required autofocus />
                    </div>
                </div>
            <!-- Password -->
                <div class="form-group mb-4">
                    <x-label for="password" :value="__('Password')" />
                    <div class="input-group">
                        <span class="input-group-text" id="basic-addon1"><span class="fas fa-unlock-alt"></span></span>
                        <x-input id="password" 
                                        type="password"
                                        name="password"
                                        placeholder="Password" 
                                        required autocomplete="current-password" />
                    </div>
                </div>
            <!-- Password Confirmation-->
                <div class="form-group mb-4">
                    <x-label for="password_confirmation" :value="__('Password Confirmation')" />
                    <div class="input-group">
                        <span class="input-group-text" id="basic-addon1"><span class="fas fa-unlock-alt"></span></span>
                        <x-input id="password" 
                                        type="password"
                                        name="password_confirmation"
                                        placeholder="Password Confirmation" 
                                        required autocomplete="current-password" />
                    </div>
                </div>
                <div class="file-field mb-4">
                    <div class="d-flex  ms-xl-3">
                       <div class="d-flex">
                          <span class="icon icon-md"><span class="fas fa-paperclip me-3"></span></span> <input type="file">
                          <div class="d-md-block text-left">
                             <div class="fw-normal text-dark mb-1">Upload Profile Pic</div>
                             <div class="text-gray small">JPG, GIF or PNG. Max size of 2 MB</div>
                          </div>
                       </div>
                    </div>
                 </div>                                        
