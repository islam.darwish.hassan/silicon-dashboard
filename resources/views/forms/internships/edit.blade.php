<div class="form-group mb-4">
    <x-label for="status" :value="__('Status')" />
    <select class="form-control form-inline" name="status">
        <option value="1" >Requested</option>
        <option value="2" >Under Review</option>
        <option value="3" >To Interview</option>
        <option value="4" >Accepted</option>
        <option value="5" >Rejected</option>
        <option value="6" >Online</option>
        <option value="7" >Offline</option>

    </select>

</div>
