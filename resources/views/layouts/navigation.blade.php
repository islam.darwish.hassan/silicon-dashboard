<nav class="navbar navbar-dark navbar-theme-primary px-4 col-12 d-md-none">
    <a class="navbar-brand me-lg-5" href="../../index.html">
        <img class="navbar-brand-dark" src="{{asset('img/brand/light.svg')}}" alt="Silicon Arena" /> <img class="navbar-brand-light" src="{{asset('img/brand/dark.svg')}}" alt="Silicon Arena" />
    </a>
    <div class="d-flex align-items-center">
        <button class="navbar-toggler d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</nav>
<x-sidebar-menu>

  <x-sidebar-item :activelink="request()->routeIs('dashboard')" >
    <a href="{{route('dashboard')}}" class="nav-link ">
      <span class="sidebar-icon"><span class="fas fa-chart-pie"></span></span>
      <span class="sidebar-text">Dashboard</span>
    </a>
  </x-sidebar-item>



  <x-sidebar-item :activelink="request()->routeIs('tasks.index')" >
    <a href="{{route('tasks.index')}}" class="nav-link">
      <span class="sidebar-icon"><span class="fas fa-tasks"></span></span>
      <span class="sidebar-text">Tasks</span>
    </a>
  </x-sidebar-item>
  <x-sidebar-item :activelink="request()->routeIs('contacts.*')" >
    <a href="{{route('contacts.index')}}" class="nav-link ">
      <span class="sidebar-icon"><span class="fas fa-address-book"></span></span>
      <span class="sidebar-text">Contacts</span>
    </a>
  </x-sidebar-item>

  <x-sidebar-multi-item  :header="__('Projects')" :activelink="request()->routeIs('projects.*')">
      <x-sidebar-item  :activelink="request()->routeIs('projects.index')"  >
        <a href="{{route('projects.index')}}" class="nav-link">
          <span class="sidebar-text">List All </span>
        </a>
      </x-sidebar-item>
  </x-sidebar-multi-item>

  <x-sidebar-item :activelink="request()->routeIs('teams.index')" >
    <a href
      class="nav-link d-flex align-items-center">
      <span class="sidebar-icon"><span class="fas fa-archive"></span></span>
      <span class="sidebar-text">Archived Projects <span>
    </a>
  </x-sidebar-item>


  <li role="separator" class="dropdown-divider mt-3 mb-3 border-black"></li>
  <x-sidebar-multi-item  :header="__('Courses')" :activelink="request()->routeIs('courses.*')" icon='fas fa-graduation-cap'>
    <x-sidebar-item  :activelink="request()->routeIs('courses.index')"  >
      <a href="{{route('courses.index')}}" class="nav-link">
        <span class="sidebar-text">List All </span>
      </a>
    </x-sidebar-item>
  </x-sidebar-multi-item>

  <x-sidebar-multi-item  :header="__('Modules')" :activelink="request()->routeIs('modules.*')" icon='fas fa-project-diagram'>
    <x-sidebar-item  :activelink="request()->routeIs('modules.index')"  >
      <a href="{{route('modules.index')}}" class="nav-link">
        <span class="sidebar-text">List All </span>
      </a>
    </x-sidebar-item>
  </x-sidebar-multi-item>


  <li role="separator" class="dropdown-divider mt-3 mb-3 border-black"></li>
  <x-sidebar-item :activelink="request()->routeIs('deals.index')" >
    <a href="{{route('deals.index')}}" class="nav-link ">
      <span class="sidebar-icon"><span class="fas fa-feather-alt"></span></span>
      <span class="sidebar-text">Deals</span>
    </a>
  </x-sidebar-item>
  <x-sidebar-multi-item  :header="__('Compaines')" :activelink="request()->routeIs('companies.*')" icon='fas fa-building'>
    <x-sidebar-item  :activelink="request()->routeIs('companies.index')"  >
      <a href="{{route('companies.index')}}" class="nav-link">
        <span class="sidebar-text">List All </span>
      </a>
    </x-sidebar-item>
  </x-sidebar-multi-item>

  <li role="separator" class="dropdown-divider mt-3 mb-3 border-black"></li>
  <x-sidebar-item :activelink="request()->routeIs('dashboard')" >
    <a href="{{route('dashboard')}}" class="nav-link ">
      <span class="sidebar-icon"><span class="fas fa-seedling"></span></span>
      <span class="sidebar-text">Financial Tracking</span>
    </a>
  </x-sidebar-item>
  <x-sidebar-item :activelink="request()->routeIs('invoices.index')" >
    <a href="{{route('invoices.index')}}" class="nav-link ">
      <span class="sidebar-icon"><span class="fas fa-receipt"></span></span>
      <span class="sidebar-text">Invoices</span>
    </a>
  </x-sidebar-item>

  <li role="separator" class="dropdown-divider mt-3 mb-3 border-black"></li>
  <x-sidebar-multi-item  :header="__('Team')" :activelink="request()->routeIs('teams.*')" icon='fas fa-user-check'>
    <x-sidebar-item  :activelink="request()->routeIs('teams.index')"  >
      <a href="{{route('teams.index')}}" class="nav-link">
        <span class="sidebar-text">List All </span>
      </a>
    </x-sidebar-item>
    <x-sidebar-item  :activelink="request()->routeIs('roles.index')"  >
        <a href="{{route('roles.index')}}" class="nav-link">
            <span class="sidebar-text">Roles </span>
          </a>
    </x-sidebar-item>

  </x-sidebar-multi-item>
  <x-sidebar-multi-item  :header="__('Applicants')" :activelink="request()->routeIs('applicants.*') || request()->routeIs('internships.*')" icon='fas fa-user-check'>
    <x-sidebar-item  :activelink="request()->routeIs('applicants.index')"  >
      <a href="{{route('applicants.index')}}" class="nav-link">
        <span class="sidebar-text">List All </span>
      </a>
    </x-sidebar-item>
    <x-sidebar-item  :activelink="request()->routeIs('internships.index')"  >
        <a href="{{route('internships.index')}}" class="nav-link">
          <span class="sidebar-text">Internships </span>
        </a>
      </x-sidebar-item>

  </x-sidebar-multi-item>

  <x-sidebar-item :activelink="request()->routeIs('quarters.index')" >
    <a href="{{route('quarters.index')}}" class="nav-link">
      <span class="sidebar-icon"><span class="fas fa-road"></span></span>
      <span class="sidebar-text">Quarters</span>
    </a>
  </x-sidebar-item>
  <x-sidebar-item :activelink="request()->routeIs('evaluations.index')" >
    <a href="{{route('evaluations.index')}}" class="nav-link">
      <span class="sidebar-icon"><span class="fas fa-chalkboard-teacher"></span></span>
      <span class="sidebar-text">Evaluations</span>
    </a>
  </x-sidebar-item>

  <li role="separator" class="dropdown-divider mt-3 mb-3 border-black"></li>
  <x-sidebar-multi-item  :header="__('Archives')" icon='fas fa-archive'>
    <x-sidebar-item  :activelink="request()->routeIs('projects.index')"  >
      <a href="{{route('projects.index')}}" class="nav-link">
        <span class="sidebar-text">2020 </span>
      </a>
      <a href="{{route('projects.index')}}" class="nav-link">
        <span class="sidebar-text">2019 </span>
      </a>
    </x-sidebar-item>
  </x-sidebar-multi-item>

</x-sidebar-menu>
