<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link
    rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css"
  />
  <link href="{{ asset('css/pace.css') }}" rel="stylesheet">
  <link href="{{ asset('css/rwd.css') }}" rel="stylesheet">
  {{-- pace --}}
  <script src="{{asset('js/pace.js')}}"></script>

</head>
<body>
    @include('layouts.navigation')
    <main class="content">
      {{$content}}
    </main>
</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

@yield('extra_scripts')
<script src="{{asset('js/rwd.js')}}"></script>

<script  src="{{asset('js/app.js')}}"></script>
<script  src="{{asset('js/volt.js')}}"></script>
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Include Choices JavaScript (latest) -->
<script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>

</html>
