
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

</head>
<body>
    <section class="d-flex align-items-center my-5 mt-lg-6 mb-lg-5">
    {{ $slot }}
    </section>
</body>
<script  src="{{asset('js/app.js')}}"></script>
<script  src="{{asset('js/volt.js')}}"></script>

</html>