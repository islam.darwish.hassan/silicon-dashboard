<x-app-layout>
    @php
    $create=['type'=>'modal','method'=>'POST','name'=>'Add New '.ucfirst(Str::singular($name)).'','route'=>[$name.'.store',[$name]],'action_route'=>[$name.'.store',[$name]]];
    @endphp
    <x-slot name="content">
        <x-top-nav>
            <div class="d-block mb-4 mb-md-0">
                <x-top-nav-bread>
                    <li class="breadcrumb-item active"><a href="{{route($name .'.index')}}">{{ucfirst($name??'')}}</a></li>
                 </x-top-nav-bread>
                <h2 class="h4 mb-0">{{ucfirst($name??'')}}</h2>
                <p class="mb-2">This {{$name??''}} dashboard to manage it .</p>
            </div>
        </x-top-nav>
        <x-action-modal-btn :operation="$create" ><span class="fas fa-plus me-2"></span></x-action-modal-btn>
        <x-action-modal :operation="$create">
                @include('forms.projects.create')
                <div class="d-grid">
                    <x-button type="submit" class="btn btn-dark">Add {{ Str::singular($name)}}</x-button>
                </div>
        </x-action-modal>
        {{-- content --}}
    </x-slot>
</x-app-layout>
