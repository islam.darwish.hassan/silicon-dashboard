<x-app-layout>
    @php
    $create=['btn_name'=>'Add Task  ','type'=>'modal','method'=>'POST','name'=>'Add New Task','route'=>[$name.'.store',[$name]],'action_route'=>[$name.'.store',[$name]]];
    @endphp
    <x-slot name="content">
        <x-top-nav>
            <div class="d-block mb-4 mb-md-0">
                <x-top-nav-bread>
                <li class="breadcrumb-item active"><a href="{{route($name .'.index')}}">{{ucfirst($name??'')}}</a></li>
                </x-top-nav-bread>
                <h2 class="h4 mb-0">{{ucfirst($name??'')}} </h2>
                <p class="mb-2">This {{$name??''}} dashboard to manage it .</p>
            </div>
        </x-top-nav>
        
        <x-action-modal-btn :operation="$create" ><span class="fas fa-plus me-2"></span></x-action-modal-btn>
        <x-action-modal :operation="$create">
                @include('forms.memebers.create')
                <div class="d-grid">
                    <x-button type="submit" class="btn btn-dark">Add New Task </x-button>
                </div>
        </x-action-modal>
        <x-searchbar/>
        <div class="nav-wrapper position-relative">
            <ul class="nav nav-pills rounded nav-fill flex-column flex-md-row">
                <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0 active" data-bs-toggle="tab" href="#">All</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" data-bs-toggle="tab" href="#">In Progress</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" data-bs-toggle="tab" href="#">Done</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link mb-sm-3 mb-md-0" data-bs-toggle="tab" href="#">Cancelled</a>
                </li>
            </ul>
        </div>
        
        <div class="accordion pb-5" id="TasksContainer">
            @foreach ($data as $item )
                @if($loop->first) 
                <x-task-accordion :task='$item' parent="TasksContainer" />
                @else
                <x-task-accordion :task='$item' parent="TasksContainer" />
                @endif
            @endforeach
       </div>
    </x-slot>
</x-app-layout>

