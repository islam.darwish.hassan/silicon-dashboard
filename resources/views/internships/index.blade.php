<x-app-layout>
    @php
        $create = ['btn_name' => 'Add Internship ', 'type' => 'modal', 'method' => 'POST', 'name' => 'Add New Internship', 'route' => [$name . '.store', [$name]], 'action_route' => [$name . '.store', [$name]]];
    @endphp
    <x-slot name="content">
        <x-top-nav>
            <div class="d-block mb-4 mb-md-0">
                <x-top-nav-bread>
                    <li class="breadcrumb-item active"><a
                            href="{{ route($name . '.index') }}">{{ ucfirst($name ?? '') }}</a></li>
                </x-top-nav-bread>
                <h2 class="h4 mb-0">{{ ucfirst($name ?? '') }} </h2>
                <p class="mb-2">This {{ $name ?? '' }} dashboard to manage it .</p>
            </div>
        </x-top-nav>

        <x-action-modal-btn :operation="$create"><span class="fas fa-plus me-2"></span></x-action-modal-btn>
        <x-action-modal :operation="$create">
            @include('forms.applicants.create')
            <div class="d-grid">
                <x-button type="submit" class="btn btn-dark">Add New Internship </x-button>
            </div>
        </x-action-modal>
        <x-searchbar route="{{ route($name . '.index') }}" />
        <x-table>
            <x-slot name="header">
                <x-table-h data-priority="0">#</x-table-h>
                <x-table-h data-priority="1">Name</x-table-h>
                <x-table-h data-priority="1">Subject</x-table-h>
                <x-table-h data-priority="1">Total</x-table-h>
                <x-table-h data-priority="1">Requested</x-table-h>
                <x-table-h data-priority="1">Under Review</x-table-h>
                <x-table-h data-priority="1">To Interview</x-table-h>
                <x-table-h data-priority="1">Accepted</x-table-h>
                <x-table-h data-priority="1">Rejected</x-table-h>
                <x-table-h data-priority="1">Online</x-table-h>
                <x-table-h data-priority="1">Offline</x-table-h>

            </x-slot>
            <x-slot name="body">
                @foreach ($data as $row)
                    <tr>
                        <x-table-b>{{ $row->id }}</x-table-b>
                        <x-table-b>{{ $row->name }}</x-table-b>
                        <x-table-b>{{ $row->subject }}</x-table-b>
                        <x-table-b><a href="{{ route($name . '.show', $row->id) }}">{{ $row->applicants_count }}</a>
                        </x-table-b>
                        <x-table-b><a
                                href="{{ route($name . '.show', [$row->id, 'status' => App\Models\Internship::STATUS_REQUESTED]) }}"><span
                                    class="text-info">{{ $row->requested_applicants_count }}</span></a></x-table-b>
                        <x-table-b><a
                                href="{{ route($name . '.show', [$row->id, 'status' => App\Models\Internship::STATUS_UNDER_REVIEW]) }}"><span
                                    class="text-warning">{{ $row->under_review_applicants_count }}</span></a>
                        </x-table-b>

                        <x-table-b><a
                                href="{{ route($name . '.show', [$row->id, 'status' => App\Models\Internship::STATUS_TO_INTERVIEW]) }}"><span
                                    class="text-warning">{{ $row->to_interview_applicants_count }}</span></a>
                        </x-table-b>
                        <x-table-b><a
                                href="{{ route($name . '.show', [$row->id, 'status' => App\Models\Internship::STATUS_ACCEPTED]) }}"><span
                                    class="text-success">{{ $row->accepted_applicants_count }}</span></a>
                        </x-table-b>
                        <x-table-b><a
                                href="{{ route($name . '.show', [$row->id, 'status' => App\Models\Internship::STATUS_REJECTED]) }}"><span
                                    class="text-danger">{{ $row->rejected_applicants_count }}</span></a>
                        </x-table-b>
                        <x-table-b><a
                            href="{{ route($name . '.show', [$row->id, 'status' => App\Models\Internship::STATUS_ONLINE]) }}"><span
                                class="text-success">{{ $row->online_applicants_count }}</span></a>
                    </x-table-b>
                    <x-table-b><a
                        href="{{ route($name . '.show', [$row->id, 'status' => App\Models\Internship::STATUS_OFFLINE]) }}"><span
                            class="text-success">{{ $row->offline_applicants_count }}</span></a>
                </x-table-b>

                    </tr>

                @endforeach

            </x-slot>
            <x-slot name="footer">
                {{ $data->links('vendor.pagination.bootstrap-4') }}

            </x-slot>

        </x-table>
    </x-slot>
</x-app-layout>
