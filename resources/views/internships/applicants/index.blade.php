<x-app-layout>
    @php
        $create = ['btn_name' => 'Add Applicant  ', 'type' => 'modal', 'method' => 'POST', 'name' => 'Add New Applicant', 'route' => [$name . '.store', [$name]], 'action_route' => [$name . '.store', [$name]]];

    @endphp
    <x-slot name="content">
        <x-top-nav>
            <div class="d-block mb-4 mb-md-0">
                <x-top-nav-bread>
                    <li class="breadcrumb-item active"><a href="{{ route('internships.index') }}">Internships</a></li>
                    <li class="breadcrumb-item active"><a
                            href="{{ route($name . '.index') }}">{{ ucfirst($name ?? '') }}</a></li>

                </x-top-nav-bread>
                <h2 class="h4 mb-0">{{ ucfirst($name ?? '') }} </h2>
                <p class="mb-2">This {{ $name ?? '' }} dashboard to manage it .</p>
            </div>

        </x-top-nav>

        <x-action-modal-btn :operation="$create"><span class="fas fa-plus me-2"></span></x-action-modal-btn>
        <x-action-modal :operation="$create">
            @include('forms.applicants.create')
            <div class="d-grid">
                <x-button type="submit" class="btn btn-dark">Add New Applicant </x-button>
            </div>
        </x-action-modal>
        <x-searchbar
            route="{{ route('internships.show', [request()->internship->id, 'status' => request()->query('status')]) }}">
            <div class="d-flex justify-content-end">
                <a href={{ route('internships.show', [request()->internship->id, 'status' => request()->query('status'), 'export' => 'csv']) }}
                    class=' btn btn-sm btn-primary   pe-1 text-white  '><span class="fas fa-file-csv me-1"></span>
                    Export Report
                </a>
        </x-searchbar>
        <x-table>
            <x-slot name="filters">
                <a href="{{ route('internships.show', [request()->internship->id]) }}"
                    class="btn btn-xs btn-primary ">All</a>
                <a href="{{ route('internships.show', [request()->internship->id, 'status' => App\Models\Internship::STATUS_REQUESTED]) }}"
                    class="btn btn-xs btn-outline-info @if (request()->query('status') ==
                    App\Models\Internship::STATUS_REQUESTED) btn-info text-white @endif">Requested</a>
                <a href="{{ route('internships.show', [request()->internship->id, 'status' => App\Models\Internship::STATUS_UNDER_REVIEW]) }}"
                    class="btn btn-xs btn-outline-warning @if (request()->query('status') ==
                    App\Models\Internship::STATUS_UNDER_REVIEW) btn-warning text-white @endif">Under Review</a>
                <a href="{{ route('internships.show', [request()->internship->id, 'status' => App\Models\Internship::STATUS_TO_INTERVIEW]) }}"
                    class="btn btn-xs btn-outline-warning @if (request()->query('status') ==
                    App\Models\Internship::STATUS_TO_INTERVIEW) btn-warning text-white @endif">To Interview</a>
                <a href="{{ route('internships.show', [request()->internship->id, 'status' => App\Models\Internship::STATUS_ACCEPTED]) }}"
                    class="btn btn-xs btn-outline-success @if (request()->query('status') ==
                    App\Models\Internship::STATUS_ACCEPTED) btn-success text-white @endif">Accepted</a>
                <a href="{{ route('internships.show', [request()->internship->id, 'status' => App\Models\Internship::STATUS_ONLINE]) }}"
                    class="btn btn-xs btn-outline-success @if (request()->query('status') ==
                    App\Models\Internship::STATUS_ONLINE) btn-success text-white @endif">Online</a>
                <a href="{{ route('internships.show', [request()->internship->id, 'status' => App\Models\Internship::STATUS_OFFLINE]) }}"
                    class="btn btn-xs btn-outline-success @if (request()->query('status') ==
                    App\Models\Internship::STATUS_OFFLINE) btn-success text-white @endif">Offline</a>
                <a href="{{ route('internships.show', [request()->internship->id, 'status' => App\Models\Internship::STATUS_REJECTED]) }}"
                    class="btn btn-xs btn-outline-danger @if (request()->query('status') ==
                    App\Models\Internship::STATUS_REJECTED) btn-danger text-white @endif">Rejected</a>

            </x-slot>
            <x-slot name="header">
                <x-table-h data-priority="0">#</x-table-h>
                <x-table-h data-priority="1">Status</x-table-h>
                <x-table-h data-priority="1">CV </x-table-h>
                <x-table-h data-priority="1">Full Name</x-table-h>
                <x-table-h data-priority="0">Email</x-table-h>
                <x-table-h data-priority="0">Phone</x-table-h>
                <x-table-h data-priority="1">Age</x-table-h>
                <x-table-h data-priority="1">City</x-table-h>
                <x-table-h data-priority="1">University</x-table-h>
                <x-table-h data-priority="1">Major</x-table-h>
                <x-table-h data-priority="1">Internships</x-table-h>

            </x-slot>
            <x-slot name="body">
                @foreach ($data as $row)
                    @php
                        $change_status = ['btn_name' => '', 'type' => 'modal', 'method' => 'PUT', 'name' => 'Change Status', 'route' => ['internship.update_status', [$row->internship(request()->internship->id)->id]], 'action_route' => ['internship.update_status', [$row->internship(request()->internship->id)->id]]];

                    @endphp

                    <tr>
                        <x-table-b>{{ $row->id }}</x-table-b>
                        <x-table-b>
                            <x-action-modal-basic-btn :operation="$change_status">
                                <small>
                                    <x-multi-value
                                        :values="['Requested','Under Review','To Interview','Accepted','Rejected','Online','Offline']"
                                        :value="$row->internship(request()->internship->id)->status"
                                        :classes="['badge bg-info p-1','badge bg-warning p-1','badge bg-warning p-1','badge bg-success p-1','badge bg-danger p-1','badge bg-success p-1','badge bg-success p-1']" />
                                </small>
                            </x-action-modal-basic-btn>
                        </x-table-b>
                        <x-table-b><a href={{ $row->cv }} target="_blank" class="text-success">View</a>
                        </x-table-b>
                        <x-table-b>{{ $row->name }}</x-table-b>
                        <x-table-b>{{ $row->email }}</x-table-b>
                        <x-table-b>{{ $row->phone }}</x-table-b>
                        <x-table-b>{{ $row->age }}</x-table-b>
                        <x-table-b>{{ $row->city }}</x-table-b>
                        <x-table-b>{{ $row->uni }}</x-table-b>
                        <x-table-b>{{ $row->major }}</x-table-b>
                        <x-table-b>
                            @foreach ($row->internships as $item)
                                <div class="badge bg-dark">{{ Str::ucfirst($item->name) }}</div>
                            @endforeach

                        </x-table-b>

                    </tr>
                    <x-action-modal :operation="$change_status">
                        @include('forms.internships.edit')
                        <div class="d-grid">
                            <x-button type="submit" class="btn btn-dark">Change Status </x-button>
                        </div>
                    </x-action-modal>

                @endforeach

            </x-slot>
            <x-slot name="footer">
                {{ $data->links('vendor.pagination.bootstrap-4') }}

            </x-slot>

        </x-table>
    </x-slot>
</x-app-layout>
