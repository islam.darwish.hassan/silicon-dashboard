<?php

namespace App\Imports;

use App\Models\Contact;
use Maatwebsite\Excel\Concerns\ToModel;

class ContactsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $contact = Contact::where('phone_number', $row[3])->first();
        if (!$contact) {
            $contact = new Contact([
                //
                'name' => $row[0].$row[1],
                'email' => $row[2],
                'phone_number' => $row[3],
            ]);
            $contact->save();
        }
        return $contact;
    }
}
