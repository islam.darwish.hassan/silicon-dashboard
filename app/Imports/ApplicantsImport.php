<?php

namespace App\Imports;

use App\Models\Applicant;
use Maatwebsite\Excel\Concerns\ToModel;

class ApplicantsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $applicant = Applicant::where('email', $row[2])->first();
        if (!$applicant) {
            $applicant = new Applicant([
                //
                'name' => $row[0],
                'age' => $row[1],
                'email' => $row[2],
                'phone' => $row[3],
                'city' => $row[4],
                'uni' => $row[5],
                'major' => $row[6],
                'year' => $row[7],
                'cv' => $row[8]
            ]);
            $applicant->save();
        }
        $applicant->internships()->syncWithoutDetaching(3);
        return $applicant;
    }
}
