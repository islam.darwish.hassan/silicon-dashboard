<?php

namespace App\Http\Controllers;

use App\Exports\ApplicantsExport;
use App\Http\Misc\Helpers\Config;
use App\Models\Applicant;
use App\Models\Internship;
use App\Models\InternshipApplicant;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InternshipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data = Internship::withCount(['applicants', 'accepted_applicants', 'rejected_applicants', 'requested_applicants', 'to_interview_applicants', 'under_review_applicants','online_applicants','offline_applicants']);
        $data = $data->searchIn(["name"], $request->search_input);
        $data = $data->paginate(Config::PAGINATION_LIMIT)->withQueryString();
        $name = 'internships';

        return view('internships.index', compact('data', 'name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Internship $internship)
    {
        //
        $data = $internship->applicants()->exactValue('status', $request->status)->with('internships');
        $data = $data->searchIn(["name", "email", "uni", 'major', 'city'], $request->search_input);

        if ($request->export) {
            $applicants =  $data->get()->makeHidden(['created_at', 'updated_at']);
            return Excel::download(new ApplicantsExport($applicants), 'applicants-' . now() . '.csv');
        }

        $data = $data->paginate(Config::PAGINATION_LIMIT)->withQueryString();
        $name = 'applicants';
        return view('internships.applicants.index', compact('data', 'name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_status(Request $request,  $id)
    {

        //
        $item = InternshipApplicant::find($id);
        $item->status = $request->status;
        $item->save();
        return back()->with(['message' => 'Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
