<?php

namespace App\Http\Controllers;

use App\Exports\ApplicantsExport;
use App\Http\Misc\Helpers\Config;
use App\Imports\ApplicantsImport;
use App\Models\Applicant;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ApplicantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data = Applicant::with('internships');
        $data = $data->searchIn(["name", "email", "uni", 'major', 'city'], $request->search_input);
        $data = $data->paginate(Config::PAGINATION_LIMIT)->withQueryString();
        $name = 'applicants';

        return view('applicants.index', compact('data', 'name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function import()
    {
        Excel::import(new ApplicantsImport, 'applicants3.csv');

        return back()->with(["message" => "Imported Successfully!"]);
    }
    public function export()
    {
        $applicants =  Applicant::get()->makeHidden(['created_at','updated_at']);
        return Excel::download(new ApplicantsExport($applicants), 'applicants-' . now() . '.csv');
    }
}
