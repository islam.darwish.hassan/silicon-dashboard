<?php

namespace App\Http\Controllers;

use App\Exports\ContactsExport;
use App\Http\Misc\Helpers\Config;
use App\Imports\ContactsImport;
use App\Models\Contact;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data = new Contact();
        $data = $data->searchIn(["name", "email", "phone_number"], $request->search_input);
        $data = $data->paginate(Config::PAGINATION_LIMIT)->withQueryString();
        $name = 'contacts';

        return view('contacts.index', compact('data', 'name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function import()
    {
        Excel::import(new ContactsImport, 'contacts.csv');

        return back()->with(["message" => "Imported Successfully!"]);
    }
    public function export()
    {
        $data =  Contact::get()->makeHidden(['created_at','updated_at']);
        return Excel::download(new ContactsExport($data), 'contacts-' . now() . '.csv');
    }
}
