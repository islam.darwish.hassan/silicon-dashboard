<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ActionModal extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $operation;
    public $large;
    public function __construct($operation ,$large=false)
    {
        //
        $this->operation=$operation;
        $this->large =$large;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.action-modal');
    }
}
