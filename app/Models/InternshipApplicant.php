<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InternshipApplicant extends Model
{
    use HasFactory;

  protected  $table='internship_applicant';

}
