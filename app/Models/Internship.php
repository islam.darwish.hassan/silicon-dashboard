<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Internship extends SuperScope
{
    use HasFactory;
    const STATUS_REQUESTED = 1;
    const STATUS_UNDER_REVIEW = 2;
    const STATUS_TO_INTERVIEW = 3;
    const STATUS_ACCEPTED = 4;
    const STATUS_REJECTED = 5;
    const STATUS_ONLINE= 6;
    const STATUS_OFFLINE = 7;

    protected $fillable = ['name', 'subject', 'description'];
    public function applicants()
    {
        return $this->belongsToMany(Applicant::class, 'internship_applicant', 'internship_id', 'applicant_id')->withTimestamps();
    }

    public function accepted_applicants()
    {
        return $this->applicants()->where('status', Internship::STATUS_ACCEPTED);
    }
    public function under_review_applicants()
    {
        return $this->applicants()->where('status', Internship::STATUS_UNDER_REVIEW);
    }
    public function rejected_applicants()
    {
        return $this->applicants()->where('status', Internship::STATUS_REJECTED);
    }
    public function to_interview_applicants()
    {
        return $this->applicants()->where('status', Internship::STATUS_TO_INTERVIEW);
    }
    public function requested_applicants()
    {
        return $this->applicants()->where('status', Internship::STATUS_REQUESTED);
    }
    public function online_applicants()
    {
        return $this->applicants()->where('status', Internship::STATUS_ONLINE);
    }
    public function offline_applicants()
    {
        return $this->applicants()->where('status', Internship::STATUS_OFFLINE);
    }

}
