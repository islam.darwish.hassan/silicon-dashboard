<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Applicant extends SuperScope
{
    use HasFactory;
    protected $fillable=['name','age','email','cv','major','year','phone','uni','city'];
    public function internships()
    {
        return $this->belongsToMany('App\Models\Internship', 'internship_applicant', 'applicant_id', 'internship_id')->withPivot(['status','id'])->withTimestamps();
    }

    public function internship($id){
        return ($this->internships()->withPivot('status')->where('internships.id',$id)->first()->pivot);
    }

}
