<?php

namespace App\Exports;

use App\Models\Applicant;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ApplicantsExport implements FromCollection, WithHeadings, WithStyles
{
    use Exportable;

    private $collection_data;
    public function __construct(Collection $collection_data)
    {
        $this->collection_data = $collection_data;
    }
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            'A'    => ['font' => ['bold' => true]],

        ];
    }

    public function headings(): array
    {
        return [
            '#',
            'Full Name',
            'Email',
            'Age',
            'Phone',
            'CV',
            'City',
            'Uni',
            'Grade'
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->collection_data;
    }
}
